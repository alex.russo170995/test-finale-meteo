import { URLs } from './config/rest-service-config'
import Cookies from "js-cookie";
import { jwtDecode } from "jwt-decode";
import { jwtExpirations } from './config/rest-service-config';


export async function registerUser(formData){
  const jsonData = JSON.stringify(formData)
  const response = await fetch(URLs.registerUrl, {
    mode: "cors",
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: jsonData
  })

  return response.status
}

export async function loginUser(formData){
  const jsonData = JSON.stringify(formData)
  const response = await fetch(URLs.loginUrl, {
    mode: "cors",
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: jsonData
  })

  return response
}

export async function getCity(name,language){
  const response = await fetch(URLs.cityUrl+'?name='+name+'&count=1&language='+language+'&format=json', {
    mode: "cors",
    method: "GET"
  })

  return response.json();
}

export async function getMeteo(lat,lon){
  const response = await fetch(URLs.meteoUrl1+'?latitude='+lat+'&longitude='+lon+URLs.meteoUrl2, {
    mode: "cors",
    method: "GET"
  })

  return response.json();
}

export function deleteCookie(){
  Cookies.remove("JWT")
}

export function setLoginCookie(jwtToken) {
  const jwtString = JSON.stringify(jwtToken.token)
  Cookies.set("JWT", jwtString, {expires: jwtExpirations.oneMonth})
  return jwtDecode(jwtString)
}

export function checkLoginCookie(){
  const loginCookie = Cookies.get("JWT")
  if(loginCookie != undefined){
    return jwtDecode(loginCookie)
  } else {
    return null
  }
}

export function getBearerToken(){
  const loginCookie = Cookies.get("JWT")
  const shortToken = loginCookie.substring(1, loginCookie.length - 1)
  if(shortToken != undefined){
    return "Bearer " + shortToken
  } else {
    return null
  }
}