export const URLs = {
    registerUrl: 'http://localhost:8080/api/utente/registrazione',
    loginUrl: 'http://localhost:8080/api/utente/login',
    cityUrl: 'https://geocoding-api.open-meteo.com/v1/search',
    meteoUrl1: 'https://api.open-meteo.com/v1/forecast',
    meteoUrl2: '&current=temperature_2m,relative_humidity_2m,wind_speed_10m'
  }

  export const jwtExpirations = {
    oneYear: new Date().getFullYear(),
    oneMonth: 31,
    oneWeek: 7,
    oneDay: 1
  }