export const rules = {
    regex: {
        nome: /^[A-Za-zÀ-Ùà-ù][a-zà-ù]*$/,
        cognome: /^[A-Za-zÀ-Ùà-ù][a-zà-ù]*$/,
        email: /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,8}$/,
        password: /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,20}/
    }
}