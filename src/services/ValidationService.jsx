import { rules } from "./config/validation-service-config";

const banners = {
  invalidPasswordRegexMatch: (
    <div>La password non rispetta il formato richiesto</div>
  ),
  invalidPasswordMatching: <div>Le password non corrispondono</div>,
  invalidLoginInformations: <div>Errore durante il login, riprovare</div>,
  invalidNomeRegexMatch: <div>Il nome non rispetta il formato richiesto</div>,
  invalidCognomeRegexMatch: <div>Il cognome non rispetta il formato richiesto</div>,
  invalidEmailRegexMatch: <div>L'email non rispetta il formato richiesto</div>
};

export const regexMatch = (validity) => {

  switch(validity){
    case 1:
      return banners.invalidNomeRegexMatch;
    case 2:
      return banners.invalidCognomeRegexMatch;
    case 3:
      return banners.invalidEmailRegexMatch;
    case 4:
      return banners.invalidPasswordRegexMatch;
    default:
      return <></>;
  }
};

export const CheckPasswordMatch = (password, confirm) => {
  if (password == confirm) {
    return true;
  } else {
    return false;
  }
};

export const areValuesMatching = (areValuesMatching) => {
  if (areValuesMatching == false) {
    return banners.invalidPasswordMatching;
  } else {
    return <></>;
  }
};

export const ValidatePassword = (passwordToValidate) => {
  return rules.regex.password.test(passwordToValidate);
};

export const ValidateNome = (text) =>{
  return rules.regex.nome.test(text);
}

export const ValidateCognome = (text) =>{
  return rules.regex.cognome.test(text);
}

export const ValidateEmail = (text) =>{
  return rules.regex.email.test(text);
}
