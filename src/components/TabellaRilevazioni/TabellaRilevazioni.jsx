export default function TabellaRilevazioni({list}){

    return(
        <>
            <table className="table mb-4">
                <thead>
                    <tr>
                        <th scope="col" className="text-center">Temperatura</th>
                        <th scope="col" className="text-center">Umidità relativa</th>
                        <th scope="col" className="text-center">Velocità del vento</th>
                        <th scope="col" className="text-center">Data</th>
                    </tr>
                </thead>
                <tbody>
                    {list!=null?
                    list.map(r => {
                        return(
                            <tr key={Math.random()}>
                                <td className="text-center">{r.temperature_2m}</td>
                                <td className="text-center">{r.relative_humidity_2m}</td>
                                <td className="text-center">{r.wind_speed_10m}</td>
                                <td className="text-center">{r.time}</td>
                            </tr>
                        )
                    }):<></>
                    }
                </tbody>
            </table>
        </>
    );
}