import { useOutlet } from "react-router-dom";
import SimpleSection from "./SimpleSection";
import Navbar from "./Navbar/Navbar";

export default function Layout(){
    const outlet = useOutlet();

  return (
    <div>
        <Navbar/>
        <SimpleSection>
            {outlet}
        </SimpleSection>
    </div>
  );
}