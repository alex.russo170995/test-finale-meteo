import { useEffect } from "react";
import MonoColumnSection from "../components/MonoColumnSection";
import RegistrationForm from "../components/RegistrationForm/RegistrationForm";

export default function Registration() {
  useEffect(() => {
    document.title = "Registrati";
  }, []);

  return (
    <>
      <MonoColumnSection
        sectionContainerClasses="container-fluid"
        sectionRowClasses="row flex-row justify-content-center"
        sectionColumnClasses="col-4 d-flex flex-column align-items-center"
      >
        <RegistrationForm/>
      </MonoColumnSection>
    </>
  );
}
