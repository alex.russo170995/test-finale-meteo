import { useContext } from "react";
import { AuthContext } from "../contexts/AuthContext";
import { Link } from "react-router-dom";

export default function Home(){
    const { currentUser } = useContext(AuthContext);

    return(
        <>
            <h1 className="text-center">Meteo</h1>
            <p className="text-center">Questo sito ti permette di effettuare delle rilevazioni meteo.</p>
            {currentUser.nome == ""?<p className="text-center"><Link to="/login">Effettua l'accesso</Link> oppure <Link to="/registrazione">registrati</Link></p>:<></>}
        </>
    )
}