import { useEffect, useState } from "react";
import { getCity, getMeteo } from "../services/RESTService";
import TabellaRilevazioni from "../components/TabellaRilevazioni/TabellaRilevazioni";

export default function Rilevazioni(){

    const [rilevazioni, setRilevazioni] = useState([]);

    const [formData,setFormData] = useState({
        city: "",
        language: ""
    });

    const fetchMeteo = async (city,language) => {
        let data=await getCity(city,language);

        if(data.results!=undefined){
            data=data.results[0];
            let meteo=await getMeteo(data.latitude,data.longitude);
            setRilevazioni([...rilevazioni, meteo.current])
        }else{
            alert("dati inseriti errati");
        }

        

        setFormData({  city: "", language: ""});
    }

    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData({ ...formData, [name]: value });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        fetchMeteo(formData.city,formData.language);
    }

    return(
        <>
            <h1 className="m-auto my-5 w-50 p-3 text-center">Lista Rilevazioni</h1>
            <form
                className="bg-light border rounded p-4"
                onSubmit={handleSubmit}
            >
                <div className="mb-3">
                    <input
                        name="city"
                        value={formData.city}
                        type="text"
                        className="form-control"
                        id="cityId"
                        placeholder="Città"
                        onChange={handleChange}
                    />
                </div>
                <div className="mb-3">
                    <input
                        name="language"
                        value={formData.language}
                        type="text"
                        className="form-control"
                        id="languageId"
                        placeholder="Lingua(es. it)"
                        onChange={handleChange}
                    />
                </div>
                <button className="btn btn-dark" type="submit">Effettua rilevazione</button>
            </form>
            <TabellaRilevazioni list={rilevazioni}/>
        </>
    )
}